package com.example.big.revizor;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;

public class SetupActivity extends AppCompatActivity {
    int widgetID = AppWidgetManager.INVALID_APPWIDGET_ID;
    Intent resultValue;
    SharedPreferences sPref;
    EditText etText;
    private String Url;
    private int Interval,MaxTimeOut,MaxMissed;
    private EditText editTextUrl,editTextInterval,editTextMaxTimeOut,editTextMaxMissed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        sPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Url = sPref.getString("url", "http://127.0.0.1:8080");
        Interval = sPref.getInt("interval", 15);
        MaxTimeOut = sPref.getInt("maxtimeout", 60);
        MaxMissed = sPref.getInt("maxmissed", 15);

        editTextUrl=(EditText)findViewById(R.id.editTextUrl);
        editTextInterval=(EditText)findViewById(R.id.editTextInterval);
        editTextMaxTimeOut=(EditText)findViewById(R.id.editTextMaxTimeOut);
        editTextMaxMissed=(EditText)findViewById(R.id.editTextMaxMissed);

        editTextUrl.setText(Url);
        editTextInterval.setText(Integer.toString(Interval));
        editTextMaxTimeOut.setText(Integer.toString(MaxTimeOut));
        editTextMaxMissed.setText(Integer.toString(MaxMissed));

        // извлекаем ID конфигурируемого виджета
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras != null) {
            widgetID = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }
        // и проверяем его корректность
        if (widgetID == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }

        // формируем intent ответа
        resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);

        // отрицательный ответ
        setResult(RESULT_CANCELED, resultValue);

    }
    public void buttonSave(View v){
        //sPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();

        ed.putString("url", editTextUrl.getText().toString());
        ed.putInt("interval",Integer.parseInt(editTextInterval.getText().toString()) );
        ed.putInt("maxtimeout",Integer.parseInt(editTextMaxTimeOut.getText().toString()) );
        ed.putInt("maxmissed",Integer.parseInt(editTextMaxMissed.getText().toString()) );

        ed.commit();
        setResult(RESULT_OK, resultValue);
        finish();
    }
    public void buttonCancel(View v){
        this.finish();
    }



}
