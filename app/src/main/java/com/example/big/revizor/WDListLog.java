package com.example.big.revizor;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RemoteViews;

import static java.lang.Math.round;

/**
 * Implementation of App Widget functionality.
 */
public class WDListLog extends AppWidgetProvider {
    final static String LOG_TAG = "WIDGET";
    private AlarmManager am;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        context.startService(new Intent(context, ParseLogService.class));
        am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent countIntent = new Intent(context, ParseLogService.class);
        PendingIntent pIntent = PendingIntent.getService(context, 0, countIntent, 0);
        am.cancel(pIntent);
        final SharedPreferences sPref =  PreferenceManager.getDefaultSharedPreferences(context);
        int  min = sPref.getInt("interval", 15);
        long updateFreq=min*60*1000;
        long timeToRefresh = SystemClock.elapsedRealtime() + updateFreq;
        //am.setRepeating(AlarmManager.ELAPSED_REALTIME,timeToRefresh,updateFreq,pIntent);
        am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,timeToRefresh,updateFreq,pIntent);
        Log.d(LOG_TAG, "UPDATE*********"+min);
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created

        //Log.d(LOG_TAG, "ENABLED");

    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
        //Log.d(LOG_TAG, "DISABLED");
    }
    public void onReceive(Context context, Intent intent) {

        RemoteViews views;
        super.onReceive(context, intent);
        String[] WStr = new String[5];
        for(int i=0;i<=4;i++){
            WStr[i]=intent.getStringExtra("PARAM"+Integer.toString(i));
            //Log.d(LOG_TAG, "PARAM "+WStr[i]);
        }

        if( "com.example.big.revizor.MYWIDGETRECEIVER".equals(intent.getAction())) {
            final SharedPreferences sPref =  PreferenceManager.getDefaultSharedPreferences(context);
            int  maxlimit = sPref.getInt("maxmissed", 15);
            //Log.d(LOG_TAG, "widget" + S2);
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            ComponentName thisWidget = new ComponentName(context, WDListLog.class);
            int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
            views = new RemoteViews(context.getPackageName(), R.layout.wdlist_log);
            int[] ids={R.id.l1,R.id.l2,R.id.l3,R.id.l4,R.id.l5};
            int count=0;
            for(int id : ids) {
                int limit=0;
                //Log.d(LOG_TAG, Integer.toString(WStr[count].split(" ").length));
                String[] DateNum = new String[2];
                DateNum=WStr[count].split(" ");
                if( DateNum.length==2){
                    //Log.d(LOG_TAG,DateNum[1]);
                    if( Integer.parseInt(DateNum[1]) >= maxlimit) limit=1;
                } else limit=1;

                if(WStr[count].equals("warning") || limit==1)
                    views.setTextColor(id, Color.parseColor("#FF0000"));
                else
                    if( Integer.parseInt(DateNum[1]) >= round(maxlimit/2) )
                        views.setTextColor(id, Color.parseColor("#F3D512"));
                    else
                        views.setTextColor(id, Color.parseColor("#FFFFFF"));

                views.setTextViewText(id, WStr[count]);
                count++;
            }
            Intent countIntent = new Intent(context, ParseLogService.class);
            PendingIntent pIntent = PendingIntent.getService(context, 0, countIntent, 0);
            views.setOnClickPendingIntent(R.id.WDlayout, pIntent);
//Привязка должна выполнятся до вызова appWidgetManager.updateAppWidget !!!
            for (int appWidgetId : appWidgetIds) {
                appWidgetManager.updateAppWidget(appWidgetId, views);
            }

        }
        //Log.d(LOG_TAG, "WIDGET OnReceive");

    }
}

