package com.example.big.revizor;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static java.lang.Math.round;

public class ParseLogService extends Service {
    final String LOG_TAG = "***** Service";
    public ParseLogService() {
    }
    @Override
    public void onCreate() {
        super.onCreate();
        //Log.d(LOG_TAG, "onCreate");
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.d(LOG_TAG, "onStartCommand");
        someTask();
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        //Log.d(LOG_TAG, "onDestroy");
    }
    @Override
    public IBinder onBind(Intent intent) {
        //Log.d(LOG_TAG, "onBind");
        return null;
    }
    public boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null && ni.isConnected()) {
            return true;
        }
        return false;
    }

    void someTask() {
        final SharedPreferences sPref =  PreferenceManager.getDefaultSharedPreferences(this);
        Thread t = new Thread(new Runnable() {
            public void run() {
                Intent intent=new Intent("com.example.big.revizor.MYWIDGETRECEIVER");
                BufferedReader reader=null;
                try {
                    String sUrl = sPref.getString("url", "http://127.0.0.1:8080");
                    URL url = new URL(sUrl);
                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                    urlc.setConnectTimeout(5000);
                    urlc.connect();
                    int Code = urlc.getResponseCode();
                    if (Code == HttpURLConnection.HTTP_OK){
                        //Log.d(LOG_TAG,"CONNECT "+sUrl);
                        reader= new BufferedReader(new InputStreamReader(urlc.getInputStream()));
                        StringBuilder buf=new StringBuilder();
                        String line=null;
                        int count=0;
                        while ((line=reader.readLine()) != null) {
                            if(count==0) isAlarm(line);
                            intent.putExtra("PARAM"+count, line);
                            //Log.d(LOG_TAG,line+"\n");
                            count++;
                        }
                    }

                }
                catch (MalformedURLException e) {for(int i = 0; i<5; i++) intent.putExtra("PARAM"+i, "warning");}
                catch (IOException e) {for(int i = 0; i<5; i++) intent.putExtra("PARAM"+i, "warning");}
                sendBroadcast(intent);
                //Log.d(LOG_TAG,"DISCONNECT");
            }
        });
        if(isConnected()){
            t.start();
        } else {
            Intent intent=new Intent("com.example.big.revizor.MYWIDGETRECEIVER");
            for(int i=0;i<5;i++) intent.putExtra("PARAM"+i, "no_network");
            sendBroadcast(intent);
        }
    }
    private void isAlarm(String s){
        SharedPreferences sPref =  PreferenceManager.getDefaultSharedPreferences(this);
        int min = sPref.getInt("maxtimeout", 60);
        int maxCountMissed = sPref.getInt("maxmissed", 15);

        int countMissed=0;
        long maxTimeInterval=min*60*1000;
        Date dateLog=new Date();
        Date dateNow=new Date();
        String[] dn=s.split(" ");
        if(dn.length!=2) { sendNotify("Что то случилось с логом!"); return; }
        try{ countMissed=Integer.parseInt(dn[1]); }
            catch(Exception e) {sendNotify("Не удается определить количество пропусков!"); return;}
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");

        try { dateLog=format.parse(dn[0]); }
            catch (Exception e) {sendNotify("В логе неверный формат даты!"); return; }
        if(countMissed >= maxCountMissed) {
            sendNotify("Большое количество пропусков! " + Integer.toString(countMissed));
            return;
        }
        long durationLog=dateNow.getTime()-dateLog.getTime();
        Log.d("DATE=======",durationLog+" "+maxTimeInterval+" "+dateNow.toString()+" "+dateLog.toString());
        if( durationLog >= maxTimeInterval){
            long duration=round(durationLog/60000);
            sendNotify("Лог не изменялся! - " + Long.toString(duration)+" мин.");
            return;
        }

    }
    private void sendNotify(String msg){
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("revizor")
                        .setContentText(msg)
                        .setAutoCancel(true)
                        .setSound(Uri.parse("android.resource://com.example.big.revizor/" + R.raw.siren));

        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }

}
